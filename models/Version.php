<?php namespace Bitcraft\Versions\Models;

use Bitcraft\SeoManager\Models\SeoTag;
use Model;
use RainLab\Translate\Models\Locale;

/**
 * Model
 */
class Version extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'bitcraft_versions_versions';

    protected $jsonable = ['data'];
    protected $appends = ['locale_versions', 'model'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public function scopeModelVersion($query, $type)
    {
        return $query->where('model_class', $type);
    }

    public function getLocaleVersionsAttribute()
    {
        $versions = [];
        if (!empty($this->data['slug'])) {
            $versions[Locale::getDefault()->code] = $this->data['slug'];
        }

        foreach ($this->data['translations'] as $locale => $translation) {
            $attributed_data = json_decode($translation['attribute_data']);
            if (@$attributed_data->slug) {
                $versions[$locale] = $attributed_data->slug;
            }
        }
        return $versions;
    }

    public function getModelAttribute() {
        return $this->model_class::find($this->model_id);
    }

    public function scopeOldVersions($query, $model)
    {
        return $query->where('version', '!=', $model->currentVersionNumber);
    }

    public function createdDate()
    {
        $date = date_create($this->created_at);
        return date_format($date, 'd.m.Y');
    }

    public static function getModelWithVersion($model_id, $version, $locale, $class)
    {
        if ($model_version = self::getModelVersion($model_id, $version, $class)) {
            return $model_version->modelWith($locale);
        }
        return null;
    }

    public static function getModelVersion($model_id, $version, $class)
    {
        return self::where('model_id', $model_id)->where('model_class', $class)->where('version', $version)->first();
    }

    public static function getVersionWithSlug($slug, $locale)
    {
        foreach (self::all() as $version) {
            if (!$version->data['published']) {
                continue;
            }

            if($version->model === null) {
                continue;
            }

            $model = $version->modelWith($locale);
            if ($model->slug === $slug) {
                return $model;
            }
        }
    }

    public function modelWith($locale)
    {
        $model = new $this->model_class();
        $seo = new SeoTag();

        foreach ($this->data as $index => $item) {
            $model->$index = $item;
            if ($locale != Locale::getDefault()->code && array_key_exists($locale, $this->data['translations'])) {
                $locale_data = json_decode($this->data['translations'][$locale]['attribute_data']);
                foreach ($locale_data as $locale_index => $locale_item) {
                    $model->$locale_index = $locale_item;
                }
            }
        }

        if ($this->data['seo_tag']) {
            foreach ($this->data['seo_tag'] as $index => $item) {
                $seo->$index = $item;

                if ($locale != Locale::getDefault()->code) {
                    if (($seo_translations = $this->data['seo_tag']['translations'])
                        && array_key_exists($locale, $seo_translations)) {
                        $locale_data = json_decode($seo_translations[$locale]['attribute_data'], true);
                        foreach ($locale_data as $locale_index => $locale_item) {
                            $seo->$locale_index = $locale_item;
                        }
                    }
                }
            }
        }

        if($model->modules !== null) {
            $model->modules = json_decode(json_encode($model->modules), true);
        }

        $model->seo_tag = $seo;

        return $model;
    }
}
