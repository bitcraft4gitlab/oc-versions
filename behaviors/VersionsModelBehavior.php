<?php namespace Bitcraft\Versions\Behaviors;

use Backend\Facades\BackendAuth;
use Bitcraft\Versions\Models\Version;
use Illuminate\Support\Facades\DB;
use RainLab\Translate\Models\Locale;

class VersionsModelBehavior extends \October\Rain\Extension\ExtensionBase
{
    protected $parent;
    protected $appends = ['hasPublishedVersion', 'currentVersionNumber', 'column_slug', 'currentVersionNumberColumn'];

    public function __construct($parent)
    {
        $this->parent = $parent;

        $parent::extend(function($model) {
            $model->hasMany['versions'] = ['Bitcraft\Versions\Models\Version', 'key' => 'model_id', 'delete' => true];

            if ($this->parent->id !== null) {
                $model->hidden = array_merge($model->hidden ?? [], ['versions']);
                $model->appends = array_merge($model->appends ?? [], $this->appends);
            }
        });
    }

    public function getColumnSlugAttribute()
    {
        if (!empty($this->parent->slug)) {
            return $this->parent->slug;
        }

        $locales = array_keys(Locale::listEnabled());

        // get translated values
        foreach ($locales as $locale) {
            if (($slug = $this->parent->getAttributeTranslated('slug', $locale)) && !empty($slug)) {
                return $slug;
            }
        }
    }

    public function frontendPath($locale) {
        $lang = '/'.$locale;
        $frontend_url = env('FRONTEND_URL');
        if (is_array($this->parent->frontend_sub_path)) {
            $frontend_path = array_key_exists($locale, $this->parent->frontend_sub_path)
                    ? $this->parent->frontend_sub_path[$locale]
                    : $this->parent->frontend_sub_path[Locale::getDefault()->code];
        } else {
            $frontend_path = $this->parent->frontend_sub_path;
        }
        $slug = $this->parent->getAttributeTranslated('slug', $locale);
        return "$frontend_url$lang$frontend_path$slug";
    }

    // Versions

    public function modelVersions() {
        return $this->parent->versions()->modelVersion(get_class($this->parent));
    }

    public function currentVersion()
    {
        return $this->parent->modelVersions()->orderBy('version', 'desc')->first();
    }

    public function getCurrentVersionNumberAttribute()
    {
        return @$this->parent->modelVersions()->orderBy('version', 'desc')->first()->version ?? 'No published version';
    }

    public function getCurrentVersionNumberColumnAttribute()
    {
        return $this->parent->hasPublishedVersion ? $this->parent->currentVersionNumber : 'No published version';
    }

    public function currentModel($locale = 'en')
    {
        if ($this->currentVersion() !== null) {
            return $this->currentVersion()->modelWith($locale);
        }
        return null;
    }

    public static function transModelWhere($slug, $locale)
    {
        return Version::getVersionWithSlug($slug, $locale);
    }

    public function getHasPublishedVersionAttribute()
    {
        if ($this->currentVersion() !== null) {
            return $this->currentModel()->published;
        }
        return false;
    }

    public function hasModules()
    {
        if (!empty($this->parent->modules)) {
            return true;
        }

        foreach (array_keys(Locale::listEnabled()) as $locale) {
            if (!empty($this->parent->getAttributeTranslated('modules', $locale))) {
                return true;
            }
        }
        return false;
    }

    public function hasEditVersion()
    {
        if ($current_version = $this->parent->currentVersion()) {
            if (strtotime($current_version->created_at) < strtotime($this->parent->updated_at)) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    public function hasValidSlug()
    {
        if (!empty($this->parent->slug)) {
            return true;
        }

        foreach (array_keys(Locale::listEnabled()) as $locale) {
            if (!empty($this->parent->getAttributeTranslated('slug', $locale))) {
                return true;
            }
        }

        return false;
    }

    public function publish()
    {
        $this->unpublishOldVersions();
        $this->createNewVersion();
    }

    public function unpublishCurrentVersion()
    {
        $version = $this->currentVersion();
        $data = $version->data;
        $data['published'] = false;
        $version->data = $data;
        $version->save();
        $this->parent->published = false;
        $this->parent->save();
    }

    public function unpublishOldVersions()
    {
        // unpublish old versions
        foreach ($this->parent->modelVersions()->get() as $version) {
            $data = $version->data;
            $data['published'] = false;
            $version->data = $data;
            $version->save();
        }
    }

    public function createNewVersion()
    {
        $this->parent->published = true;
        $this->parent->save();

        $new_version = new Version();
        $new_version->model_id = $this->parent->id;
        $new_version->model_class = get_class($this->parent);
        $new_version->version = count($this->parent->modelVersions()->get()) + 1;
        $new_version->data = $this->encode();
        $new_version->user = BackendAuth::getUser()->login;
        $new_version->save();
    }

    public function publishVersion($version): bool
    {
        if ($old_version = Version::getModelVersion($this->parent->id, $version)) {
            $this->unpublishOldVersions();
            // get model data from old version
            $this->parent->modules = $old_version->data['modules'];
            $this->parent->slug = $old_version->data['slug'];
            $this->parent->save();

            foreach (array_keys(Locale::listEnabled()) as $locale) {
                if (array_key_exists($locale, $old_version->data['translations']) && $locale_data = $old_version->data['translations'][$locale]) {
                    $obj = Db::table("rainlab_translate_attributes")
                        ->where("locale", $locale)
                        ->where("model_id", $this->parent->id)
                        ->where("model_type", get_class($this));

                    if ($obj->count() > 0) {
                        $obj->update(["attribute_data" => $locale_data['attribute_data']]);
                    } else {
                        DB::table('rainlab_translate_attributes')->insert([
                            [
                                'locale' => $locale,
                                'model_id' => $this->parent->id,
                                'model_type' => get_class($this),
                                'attribute_data'  => $locale_data['attribute_data']
                            ]
                        ]);
                    }
                } else {
                    Db::table("rainlab_translate_attributes")
                        ->where("locale", $locale)
                        ->where("model_id", $this->parent->id)
                        ->where("model_type", get_class($this))->delete();
                }
            }

            // remove appended attributes before save
            $attributes_to_remove = $this->appends;
            array_push($attributes_to_remove, 'updated_at', 'deleted_at', 'created_at');
            foreach ($attributes_to_remove as $append_attribute) {
                unset($this->parent->$append_attribute);
            }


            if ($old_version->data['seo_tag']) {
                // get seo data from old version
                $this->parent->seo_tag->meta_title = $old_version->data['seo_tag']['meta_title'];
                $this->parent->seo_tag->meta_description = $old_version->data['seo_tag']['meta_description'];
                $this->parent->seo_tag->meta_keywords = $old_version->data['seo_tag']['meta_keywords'];
                $this->parent->seo_tag->canonical_url = $old_version->data['seo_tag']['canonical_url'];
                $this->parent->seo_tag->redirect_url = $old_version->data['seo_tag']['redirect_url'];
                $this->parent->seo_tag->robot_index = $old_version->data['seo_tag']['robot_index'];
                $this->parent->seo_tag->robot_follow = $old_version->data['seo_tag']['robot_follow'];
                $this->parent->seo_tag->robot_advanced = $old_version->data['seo_tag']['robot_advanced'];
                $this->parent->seo_tag->og_title = $old_version->data['seo_tag']['og_title'];
                $this->parent->seo_tag->og_description = $old_version->data['seo_tag']['og_description'];
                $this->parent->seo_tag->og_type = $old_version->data['seo_tag']['og_type'];
                $this->parent->seo_tag->og_image = $old_version->data['seo_tag']['og_image'];
                $this->parent->seo_tag->save();
                foreach (array_keys(Locale::listEnabled()) as $locale) {
                    if (array_key_exists($locale, $old_version->data['seo_tag']['translations'])
                        && $locale_seo_data = $old_version->data['seo_tag']['translations'][$locale]) {
                        $obj = Db::table("rainlab_translate_attributes")
                            ->where("locale", $locale)
                            ->where("model_id", $this->parent->seo_tag->id)
                            ->where("model_type", get_class($this->parent->seo_tag));

                        if ($obj->count() > 0) {
                            $obj->update(["attribute_data" => $locale_seo_data['attribute_data']]);
                        }
                    }
                }
            }

            // create new version
            $this->createNewVersion();
            return true;
        }

        return false;
    }

    public function encode(): array
    {
        $this->parent->published = true;
        $array = $this->parent->toArray();
        $array['seo_tag'] = $this->parent->seo_tag ? $this->parent->seo_tag->encode() : null;

        $translations = [];
        foreach ($this->parent->translations->toArray() as $item) {
            $translations[$item['locale']] = $item;
        }
        $array['translations'] = $translations;
        return $array;
    }
}
