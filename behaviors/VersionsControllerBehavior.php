<?php namespace Bitcraft\Versions\Behaviors;

use Backend\Classes\ControllerBehavior;

class VersionsControllerBehavior extends ControllerBehavior
{
    /**
     * @inheritDoc
     */
    protected $requiredProperties = ['versionsConfig'];

    /**
     * @var array Versions definitions, keys for alias and value for configuration.
     */
    protected $versionsDefinitions;

    /**
     * @var string The primary versions alias to use. Default: list
     */
    protected $primaryDefinition;

    /**
     * @var array Versions configuration, keys for alias and value for config objects.
     */
    protected $versionsConfig = [];

    /**
     * @var object Class of the related model
     */
    protected $class;

    /**
     * @var bool Check if form is already extended
     */
    protected $extended = false;

    /**
     * @var array Configuration values that must exist when applying the primary config file.
     * - modelClass: Class name for the model
     */
    protected $requiredConfig = ['modelClass'];

    public function __construct($controller)
    {
        parent::__construct($controller);

        /*
         * Extract versions definitions
         */
        if (is_array($controller->versionsConfig)) {
            $this->versionsDefinitions = $controller->versionsConfig;
            $this->primaryDefinition = key($this->versionsDefinitions);
        }
        else {
            $this->versionsDefinitions = ['versions' => $controller->versionsConfig];
            $this->primaryDefinition = 'versions';
        }

        /*
         * Build configuration
         */
        $this->setConfig($this->versionsDefinitions[$this->primaryDefinition], $this->requiredConfig);

        /*
         * Get the class
         */
        $definition = null;
        if (!$definition || !isset($this->versionsDefinitions[$definition])) {
            $definition = $this->primaryDefinition;
        }

        $versionsConfig = $this->versionsGetConfig($definition);
        $this->class = $versionsConfig->modelClass;

        /*
         * Merge Releation config with versions
         * If not already set implement RelationController
         */
        // Implement relationController if not already implemented
        if (!$controller->isClassExtendedWith('Backend.Behaviors.RelationController')) {
            $controller->implement[] = 'Backend.Behaviors.RelationController';
        }

        // Define relationConfig if not defined
        if (!isset($controller->relationConfig)) {
            $controller->addDynamicProperty('relationConfig');
        }

        // Splice in configuration safely
        $myConfigPath = '$/bitcraft/versions/configs/config_relation.yaml';
        $controller->relationConfig = $controller->mergeConfig(
            $controller->relationConfig,
            $myConfigPath
        );

        /*
         * Add Versions Toolbar
         */
        $controller::extendFormFields(function ($form, $model, $context) {
            if ($form->getContext() != 'update' ||$this->extended) {
                return;
            }
            $this->extended = true;

            $form->addFields([
                'toolbar' => [
                    'type' => 'partial',
                    'path' => '$/bitcraft/versions/partials/publish_toolbar.htm',
                ],
            ]);

            $form->addTabFields([
                'versions' => [
                    'label' => 'Versions',
                    'span' => 'full',
                    'tab' => 'Versions',
                    'type'  => 'partial',
                    'path' => '~/plugins/bitcraft/versions/models/version/_versions.htm'
                ]
            ]);
        });
    }

    public function onPublish($id)
    {
        if ($model = $this->class::find($id)) {
            $model->publish();
            \Flash::success('Model published and new version created!');
            return back();
        } else {
            \Flash::error('Error!');
            return back();
        }
    }

    public function onUnpublishCurrentVersion($id)
    {
        if ($model = $this->class::find($id)) {
            $model->unpublishCurrentVersion();
            \Flash::success('Unpublished current version');
            return back();
        } else {
            \Flash::error('Error!');
            return back();
        }
    }

    public function onPublishOldVersion($id)
    {
        if (($model = $this->class::find($id)) && $model->publishVersion(post('version'))) {
            \Flash::success('Old version published');
            return back();
        }

        \Flash::error('Error!');
        return back();
    }

    /**
     * Returns the configuration used by this behavior.
     * @return \Backend\Classes\WidgetBase
     * @throws \SystemException
     */
    public function versionsGetConfig($definition = null)
    {
        if (!$definition) {
            $definition = $this->primaryDefinition;
        }

        if (!$config = array_get($this->versionsConfig, $definition)) {
            $config = $this->versionsConfig[$definition] = $this->makeConfig($this->versionsDefinitions[$definition], $this->requiredConfig);
        }

        return $config;
    }
}
