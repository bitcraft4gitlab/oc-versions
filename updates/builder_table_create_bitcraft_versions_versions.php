<?php namespace Bitcraft\Versions\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBitcraftVersionsVersions extends Migration
{
    public function up()
    {
        Schema::create('bitcraft_versions_versions', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('model_id')->unsigned();
            $table->string('model_class');
            $table->integer('version')->unsigned();
            $table->text('data')->nullable();
            $table->string('user')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bitcraft_versions_versions');
    }
}
